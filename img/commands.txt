
#### pictures list for md files :

from:
20160123_213020.jpg
20160123_213029.jpg
20160123_213042.jpg
20160123_213056.jpg
to:
|![ text](img/20160123_213020.jpg) |![ text](img/20160123_213029.jpg) | 
|![ text](img/20160123_213042.jpg) |![ text](img/20160123_213056.jpg) | 

## command
ls -1 2016* | awk '{ if (NR%2) { printf "|![ text](img/" $1 ") |" } else { printf "![ text](img/" $1 ") | \n"} }'

### setup

* decoupeuse plasma 50A 
ici limité a 16/18A car limité par la ligne electrique
* compresseur
* meuleuse 

| 16-07-2015 ||
|---|---|
|![](img/resised-IMG_20150716_153541.jpg)|![](img/resised-IMG_20150716_160553.jpg)


## 1er jour

* nettoyage de la porte de securité
* decoupe encadrement porte
* degaushissage traverse sous container
* ebarbage des restes de coupe

* present : Dr Xano, frtk, metaB, pg

| 20-07-2015 | |
|---|---|
|![](img/resised-IMG_20150720_115037.jpg)|![](img/resised-IMG_20150720_115014.jpg)



## 2eme jour

* tracé des decoupes
* découpe de 3 fenetres 
* present : Dr Xano, frtk, metaB, pg


| 21-07-2015 ||
|---|---|
|![ text](img/jour2/resised-IMG_20150721_115533.jpg)|![ text](img/jour2/resised-IMG_20150721_115551.jpg)
|![ text](img/jour2/resised-IMG_20150721_115556.jpg)|![ text](img/jour2/resised-IMG_20150721_115610.jpg)
|![ text](img/jour2/resised-IMG_20150721_115935.jpg)|![ text](img/jour2/resised-IMG_20150721_120010.jpg)
|![ text](img/jour2/resised-IMG_20150721_120024.jpg)|![ text](img/jour2/resised-IMG_20150721_120042.jpg)

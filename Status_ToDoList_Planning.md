### ToDoList
* Achats: 
    - VMC (au fond) + boite aeration (a l'entree) (ramener/echanger ceux qu'on a achete?)
    - 4 radiateurs et thermostat
    - boitiers prises, connecteurs ?
    - composant boitier elec (disjoncteurs, etc...)
    - eclairage plafond
    - plinthes ?
    - finition (bord fenetres plastique?)
    - platre enduit finition plaquo
    - spatules enduit plaquo (1 petite et 1 grande)
    - bande raccord pour enduit plaquo
    
    
* A faire:
    - coffrage plaquo pour les encadrements des fenetres
    - decrocher le cable fibre et le ramener vers le DD2 (a faire pour le lundi au plus tard)
    - poser structure autour du bloc reseau
    - aggrandir le trou acces cable elec exterieure
    - raccorder fibre optique au DD2 avec Obinou (a confirmer)
    - raccorder electricite exterieure au container
    - acheter les elements du tableau elec 
    - finir plaquo autour du tableau elec
    - monter tableau elec et composants
    - poser prise elec
    - finir structure plaquo autour du bloc reseau
    - poser le bloc reseau
    - poser boitier prise reseau
    - ...
    - installer VMC
    - refaire trou et poser grille aeration
    - ...
    - systeme d'ouverture pour la porte de secours (a raccorder pour finir la porte)
    - ...
    - raccorder elec exterieure au boitier 
    - raccorder fibre optique au switch
    - ....
    - mastic pour les raccords mur/plafond
    - poser enduit plaquo
    - peindre
    - fixer lumiere sortie de secours
    - ...
    - poser radiateurs et thermostat
    -  ...
    -  trier le bordel dans le container actuel
    -  demenager
    -  profit?

* A faire plus tard:
    -  baie vitree a l'entree
    -  isolation porte cote gauche (caisson)
    -  caisson autour des poteaux centraux et des parties metalliques entre les 2 containers
    -  renfort IPN au niveau des poteaux dans le container  pour se passer des poteaux?
    -  ...
### DD2 work in progress picture gallery

| Travaux 11/04/2015 |  |
|---|---|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150411_191832.jpg)|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150411_191930.jpg)

| Travaux 16/07/2015 ||
|---|---|
|![](img/resised-IMG_20150716_153541.jpg)|![](img/resised-IMG_20150716_160553.jpg)


| Travaux 20/07/2015 | |
|---|---|
|![](img/resised-IMG_20150720_115037.jpg)|![](img/resised-IMG_20150720_115014.jpg)


| Travaux 21/07/2015 ||
|---|---|
|![ text](img/jour2/resised-IMG_20150721_115533.jpg)|![ text](img/jour2/resised-IMG_20150721_115551.jpg)
|![ text](img/jour2/resised-IMG_20150721_115556.jpg)|![ text](img/jour2/resised-IMG_20150721_115610.jpg)
|![ text](img/jour2/resised-IMG_20150721_115935.jpg)|![ text](img/jour2/resised-IMG_20150721_120010.jpg)
|![ text](img/jour2/resised-IMG_20150721_120024.jpg)|![ text](img/jour2/resised-IMG_20150721_120042.jpg)

| Travaux 23/07/2015 |  |
|---|---|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150723_132819.jpg)|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150723_132846.jpg)|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150723_132907.jpg)|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150723_133021.jpg)

| Travaux 10/08/2015 |  |
|---|---|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150810_194848.jpg)| |

| Travaux 09/09/2015 |  |
|---|---|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150909_155506.jpg)| |

| Travaux 12/09/2015 |  |
|---|---|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150912_173319.jpg)|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150912_173329.jpg)|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150912_173349.jpg)|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150912_181853.jpg)|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150912_192037.jpg)|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150912_192051.jpg)|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150912_192100.jpg)|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150912_192117.jpg)|

| Travaux 27/09/2015 |  |
|---|---|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150927_184101.jpg)|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150927_184116.jpg)|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150927_184135.jpg)|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150927_184150.jpg)|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150927_184216.jpg)|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150927_184234.jpg)|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20150927_190911.jpg)| |

| Travaux 10/10/2015 |  |
|---|---|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20151010_192758.jpg)|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20151010_192817.jpg)
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20151010_192838.jpg)|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20151010_192854.jpg) 
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20151010_192923.jpg)|

| Travaux 11/10/2015 |  |
|---|---|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20151011_200525.jpg)|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20151011_200544.jpg)|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20151011_200554.jpg)|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20151011_200622.jpg)|
|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20151011_204432.jpg)|![alt text](https://git.tetalab.org/tetalab/Double_dragon2/raw/master/img/20151011_204452.jpg)|


| Travaux 16/10/2015 |  |
|---|---|
|![ text](img/resize-20151016_171727.jpg) |![ text](img/resize-20151016_180703.jpg) |
|![ text](img/resize-20151016_180727.jpg) |![ text](img/resize-20151016_191250.jpg) |
|![ text](img/resize-20151016_193935.jpg) |![ text](img/resize-20151016_202004.jpg) |
|![ text](img/resize-20151016_204047.jpg) |![ text](img/resize-20151016_204103.jpg) |
|![ text](img/resize-20151016_205512.jpg) |![ text](img/resize-20151016_205519.jpg) |
|![ text](img/resize-20151016_205527.jpg) |![ text](img/resize-20151016_215447.jpg) |


| Travaux 18/10/2015 |  |
|---|---|
|![ text](img/resize-20151018_174309.jpg) |![ text](img/resize-20151018_174350.jpg) |
|![ text](img/resize-20151018_174412.jpg) |![ text](img/resize-20151018_180551.jpg) |
|![ text](img/resize-20151018_195949.jpg) |![ text](img/resize-20151018_200009.jpg) | 
|![ text](img/resize-20151018_202737.jpg) |![ text](img/resize-20151018_202753.jpg) |


| Travaux 19/10/2015 |  |
|---|---|
|![ text](img/resize-20151019_101750.jpg) |![ text](img/resize-20151019_101759.jpg) |
|![ text](img/resize-20151019_112009.jpg) |![ text](img/resize-20151019_112022.jpg) |
|![ text](img/resize-20151019_122809.jpg) |![ text](img/resize-20151019_122827.jpg) |
|![ text](img/resize-20151019_122847.jpg) |![ text](img/resize-20151019_123205.jpg) |
|![ text](img/resize-20151019_153329.jpg) | |


| Travaux 21/10/2015 |  |
|---|---|
|![ text](img/20151021_185232.jpg) |![ text](img/20151021_185254.jpg) |
|![ text](img/20151021_210412.jpg) |![ text](img/20151021_210425.jpg) |


| Travaux 22/10/2015 |  |
|---|---|
|![ text](img/20151022_165040.jpg) | |


| Travaux 23/10/2015 |  |
|---|---|
|![ text](img/20151023_193359.jpg) | |


| Travaux 24/10/2015 |  |
|---|---|
|![ text](img/20151024_094401.jpg) |![ text](img/20151024_144207.jpg) |
|![ text](img/20151024_144220.jpg) |![ text](img/20151024_150408.jpg) |
|![ text](img/20151024_150421.jpg) |![ text](img/20151024_151239.jpg) |
|![ text](img/20151024_151255.jpg) |![ text](img/20151024_153406.jpg) |
|![ text](img/20151024_160144.jpg) |![ text](img/20151024_173138.jpg) |
|![ text](img/20151024_181230.jpg) |![ text](img/20151024_192043.jpg) |
|![ text](img/20151024_193642.jpg) |![ text](img/20151024_200307.jpg) |
|![ text](img/20151024_200943.jpg) | |


| Travaux 25/10/2015 |  |
|---|---|
|![ text](img/20151025_140308.jpg) |![ text](img/20151025_140319.jpg) |
|![ text](img/20151025_140345.jpg) |![ text](img/20151025_144703.jpg) |
|![ text](img/20151025_152811.jpg) |![ text](img/20151025_154423.jpg) |
|![ text](img/20151025_165034.jpg) |![ text](img/20151025_165054.jpg) |
|![ text](img/20151025_171731.jpg) |![ text](img/20151025_210746.jpg) | 


| Travaux 26/10/2015 |  |
|---|---|
|![ text](img/20151026_131413.jpg) |![ text](img/20151026_135215.jpg) |
|![ text](img/20151026_140440.jpg) |![ text](img/20151026_150232.jpg) |
|![ text](img/20151026_150239.jpg) |![ text](img/20151026_150259.jpg) |
|![ text](img/20151026_150317.jpg) | |


| Travaux 27/10/2015 |  |
|---|---|
|![ text](img/20151027_182612.jpg) |![ text](img/20151027_182632.jpg) |
|![ text](img/20151027_182642.jpg) |![ text](img/20151027_182649.jpg) |
|![ text](img/20151027_182658.jpg) | |


| Travaux 28/10/2015 |  |
|---|---|
|![ text](img/20151028_155351.jpg) |![ text](img/20151028_215710.jpg) |
|![ text](img/20151028_215722.jpg) |![ text](img/20151028_215744.jpg) |
|![ text](img/20151028_215753.jpg) |![ text](img/20151028_233112.jpg) |
|![ text](img/20151028_233213.jpg) | |


| Travaux 01/11/2015 |  |
|---|---|
|![ text](img/20151101_175556.jpg) |![ text](img/20151101_191708.jpg) |


| Travaux 03/11/2015 |  |
|---|---|
|![ text](img/20151103_000420.jpg) |![ text](img/20151103_000427.jpg) |
|![ text](img/20151103_000448.jpg) |![ text](img/20151103_000455.jpg) |
|![ text](img/20151103_000503.jpg) |![ text](img/20151103_230050.jpg) |
|![ text](img/20151103_230100.jpg) |![ text](img/20151103_230113.jpg) |
|![ text](img/20151103_230133.jpg) | | 


| Travaux 04/11/2015 |  |
|---|---|
|![ text](img/20151104_173252.jpg)|![ text](img/20151104_173729.jpg)|
|![ text](img/20151104_173742.jpg) |![ text](img/20151104_175850.jpg)|
|![ text](img/20151104_182442.jpg) |![ text](img/20151104_182448.jpg)|
|![ text](img/20151104_185409.jpg) |![ text](img/20151104_185416.jpg)|
|![ text](img/20151104_195624.jpg) |![ text](img/20151104_213751.jpg)|
|![ text](img/20151104_230233.jpg) |![ text](img/20151104_230315.jpg)|
|![ text](img/20151104_230323.jpg) |![ text](img/20151104_230336.jpg)|


| Travaux 05/11/2015 |  |
|---|---|
|![ text](img/20151105_012243.jpg) | ![ text](img/20151105_012302.jpg) |
|![ text](img/20151105_012319.jpg) | ![ text](img/20151105_185146.jpg) |
|![ text](img/20151105_185153.jpg) | ![ text](img/20151105_185208.jpg)|
|![ text](img/20151105_185227.jpg) | | 


| Travaux 07/11/2015 |  |
|---|---|
|![ text](img/20151107_185014.jpg) |![ text](img/20151107_185028.jpg) |
|![ text](img/20151107_185046.jpg) |![ text](img/20151107_202202.jpg) |
|![ text](img/20151107_202218.jpg) |![ text](img/20151107_204753.jpg) | 
|![ text](img/20151107_204831.jpg) |![ text](img/20151107_210308.jpg) |







## Liste de materiel a recuperer/acheter

## Meubles:
- 1 canape

## Materiel de mesure:
- sondes oscilloscope
- multimetre

## Soudure:
- fers a souder pour les solder station

## Ecrans/VP:
- VP + ecran

## Materiel Computing:
- cle USB (pour faire cle bootables et/ou du stockage)

## MicroProc:
- rapis0 ?
- qques arduinos


*** Dates Travaux 

* Samedi 17 Octobre 2015:
    - matin: achats
        - ~~1 boite vis perforante longue~~
        - ~~1 boite vis perforante courte~~
        - ~~1 boite vis bois 45/50 mm~~ boite dispo au container
        - ~~rail plaquo plafond~~


* Dimanche 18 Octobre 2015:
    - apres-midi: 
        - ~~finir pose des 2 dernieres fenetres~~ 
        - ~~Regler structure des rails autour du panneau elec~~
        - ~~Continuer pose des panneaux de plaquo manquants~~ 
        - ~~Commencer la structure du plafond~~


* Lundi 19 Octobre 2015:
    - matinee: achats
        - ~~laine de verre pour plafond~~
        - ~~plaquo plafond(30 plaques)~~
        - ~~rails plafond (70 + 6)~~ 
    - aprem: 
        - ~~tirer gaines elec plafond (lumiere)~~
        - ~~pose structure des rails plafond gauche~~
        - ~~mesurer et decouper rails transverses plafond gauche~~
        - ~~avancer pose des rails transverses plafond gauche~~    
        
        
* Mardi 20 Octobre 2015:
    - ~~tirer gaine lumiere plafond gauche~~

    
* Mercredi 21 Octobre:
    - ~~mesurer et decouper les barres transverses manquantes plafond gauche~~ 
    - ~~poser plaquo sur mur derriere boitier elec (entier)~~
    - ~~redresser les rails pour rectifier a 2.50m~~
    - ~~finir rails transverse plafond a gauche~~
    - ~~trouver solution pour rattraper les aiguilles dans les 2 gaines reseau~~
    - ~~ramener bloc reseau~~
    - ~~avancer pose ceinture cote droit~~
    - ~~avancer decoupe des rails transverse cote droit~~ 
  
  
* Jeudi 22 Octobre: 
    - ~~acheter cable rj45~~ 
    - ~~avancer pose des rails transverse plafond droite~~
    

* Vendredi/Samedi/Dimanche 23/24/25 Octobre:
    - matin & aprem:
        - ~~tirer et agencer gaine lumiere plafond droit~~
        - ~~passer les cables rj45 dans les gaines~~
        - ~~caller les gaines derriere les rails plaquo~~
        - ~~prendre mesure tous les 50cm des 2 cotes de la structure rail~~ 
        - ~~redresser les barres decallees~~
        - ~~mesurer la taille et marquer les barres transverses plafond droit pour la decoupe~~
        - ~~decoupe des barres transverses manquantes plafond droit~~
        - ~~arranger les gaines reseau et elec au niveau des rails~~
        - ~~poser plaquo haut du bloc reseau et lamelle qui manque~~
        - ~~finir structure rails plafond a droite~~
        - ~~Poser les rails transverse manquant plafond droit~~
        - ~~poser isolation plafond gauche~~
        - ~~poser plaquo plafond gauche~~         
        - ~~preparer les gaines elec plafond pour les lumieres~~

        
* Semaine 26/30 Octobre:
    - Lundi:
        - ~~poser isolation plafond droit~~
        - ~~poser plaquo plafond droit~~
           
    - Apres lundi:
        - ~~coffrage plaquo pour les encadrements des fenetres~~
        - ~~poser structure autour du bloc reseau~~
        - ~~recuperer un switch~~ (solargate dispo au container) 
        - ~~finir plaquo plafond gauche~~
        - ~~poser isolation plafond droit~~
        - ~~poser plaquo plafond droit~~
    
    - Mardi:
        - ~~coffrage plaquo pour les encadrements des fenetres~~
    
    - Mercredi:
        - Achats:
            - ~~enduit plaquo~~
            - ~~spatules (1 petite et 1 grande)~~
            - ~~bande raccord~~ 
            - ~~composants pour boitier elec~~
        - Travaux:
            - ~~commencer a poser l'enduit~~
            - ~~mastic pour les raccords mur/plafond~~
            - ~~discussion avec Clement et Michael pour le raccord a l'elec externe~~
    - Jeudi et Vendredi:
            - ~~pose enduit plaquo~~
            
    - Reste:
        - ~~acheter les elements du tableau elec~~
        - ~~finir plaquo autour du tableau elec~~
        - ~~monter tableau elec et composants~~
        - ~~poser prise elec~~
        - ~~finir structure plaquo autour du bloc reseau~~
        - ~~poser structure du bloc reseau~~
        - ~~poser les prises reseau~~
        - ~~acheter VMC~~
        - ~~installer VMC~~
        - ~~poser enduit plaquo~~
        - ~~peindre~~
        - ~~fixer lumiere sortie de secours~~
        - ~~acheter radiateur et thermostat~~
        - ~~poser 1 radiateur~~
        
        
* Jeudi/Vendredi 06/05 Novembre
    - ~~finir enduit plaquo~~
    - ~~premiere couche peinture sur les murs~~


* Samedi et Dimanche 07/08 Novembre:
    - ~~Peinture murs derniere couche~~
    - ~~peinture plafond premiere couche~~
    - ~~peinture plafond deuxieme couche~~


* Lundi 09 Novembre:
    - Achats:
        - ~~cable 5G6 (30m)~~
    - Location: 
        - ~~ponceuse sol pour mardi 10 novembre~~
    - Travaux:
        - recuperer fibre optique
        - laver/gratter sol
        - clean du chantier 
        - decouper morceau plaquo de la taille du dessus du tableau elec (la peindre)
        - repeindre les bavures


* Mardi 10 Novembre:
    - installer cable electrique 5G6 le long des poutres
    - poncer le sol


* Mercredi 11 novembre:
    - poser 2 radiateurs(th)
    - finir baie reseau
    - peindre sol
    - finir boitier reseau rj45
    - finir bloc issue secours 
    - acheter disjoncteur 2A gewiss
    - tirer cable 5g6 jusqu au tableau exterieur
    - raccorder si cable tire
    - poser plaquo au dessu du tableau 
    - poser vmc
    

* Lundi 16 Novembre:
    - Obinou passe raccorder fibre optique au boitier


